//
//  DBManager.swift
//  iGenius
//
//  Created by Angelos Staboulis on 10/08/2018.
//  Copyright © 2018 Angelos Staboulis. All rights reserved.
//

import Foundation
import RealmSwift

class DBManager {
    
    private var   database:Realm
    
    static let   sharedInstance = DBManager()
    
    private init() {
        
        database = try! Realm()
        
    }
    
    func getDataFromDB() ->   Results<Image> {
        
        let results: Results<Image> =   database.objects(Image.self)
        
        return results
        
    }
    
    func addData(object: Image)   {
        
        try! database.write {
            
            database.add(object, update: true)
            
            print("Added new image")
            
        }
        
    }
    
    func deleteAllFromDatabase()  {
        
        try!   database.write {
            
            database.deleteAll()
            
        }
        
    }
    
    func deleteFromDb(object: Image)   {
        
        try!   database.write {
            
            database.delete(object)
            
        }
        
    }
    
}
