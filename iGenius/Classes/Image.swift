//
//  Image.swift
//  iGenius
//
//  Created by Angelos Staboulis on 10/08/2018.
//  Copyright © 2018 Angelos Staboulis. All rights reserved.
//

import Foundation
import RealmSwift
class Image: Object {
    @objc public dynamic var id: String = ""
    @objc public dynamic var url: String = ""
    @objc public dynamic var likes: String = ""
    @objc public dynamic var comments: String = ""
    override class func primaryKey() -> String? {
        return "id"
    }
}
