//
//  TableViewCellGenius.swift
//  iGenius
//
//  Created by Angelos Staboulis on 09/08/2018.
//  Copyright © 2018 Angelos Staboulis. All rights reserved.
//

import UIKit

class TableViewCellGenius: UITableViewCell {
    @IBOutlet weak var lblCountLikes: UILabel!
    @IBOutlet weak var loadImage: UIImageView!
    
    @IBOutlet weak var lblCountComments: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
