//
//  TableViewInstagram.swift
//  iGenius
//
//  Created by Angelos Staboulis on 09/08/2018.
//  Copyright © 2018 Angelos Staboulis. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SDWebImage
import CollieGallery


class TableViewInstagram: UITableViewController {
    
    var count=Int()
    
    var getCount=Int()

    var image=Image()
    
    var urls=[String]()
    
    var likes=[String]()
    
    var comments=[String]()
    
    var db=DBManager.sharedInstance
    func requestGETURL(_ strURL: String, success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void) {
            Alamofire.request(strURL).responseJSON { (responseObject) -> Void in
                if responseObject.result.isSuccess {
                    let resJson = JSON(responseObject.result.value!)
                    success(resJson)
                }
                if responseObject.result.isFailure {
                    let error : Error = responseObject.result.error!
                    failure(error)
                }
            }
        
        
    
    }
 
    func saveDataWithoutInternet(){
        let todoEndpoint = "https://api.instagram.com/v1/users/self/media/recent/?access_token=1403616281.1677ed0.4711f70fb8344bde931a09e56ef70514"
        self.requestGETURL(todoEndpoint, success: {
            (JSONResponse) -> Void in
            let json=JSON(JSONResponse)
            let saveImage=Image()
            var counter=Int()
            while counter<json["data"].count{
                self.urls.append(json["data"][counter]["images"]["standard_resolution"]["url"].string!)
                self.likes.append(json["data"][counter]["likes"]["count"].stringValue)
                self.comments.append(json["data"][counter]["comments"]["count"].stringValue)
                counter=counter+1
             }
            for j in 0...self.urls.count{
                saveImage.id=String(j)
                saveImage.url=self.urls[j]
                saveImage.likes=self.likes[j]
                saveImage.comments=self.comments[j]
                DBManager.sharedInstance.addData(object: saveImage)
            }
        }) {
            (error) -> Void in
            print(error)
        }
    }
    func getPictureUrl(){
        let todoEndpoint = "https://api.instagram.com/v1/users/self/media/recent/?access_token=1403616281.1677ed0.4711f70fb8344bde931a09e56ef70514"
        self.requestGETURL(todoEndpoint, success: {
            (JSONResponse) -> Void in
            let json=JSON(JSONResponse)
            var count=Int()
            while count<json["data"].count{
                let defaults = UserDefaults.standard
                defaults.set(json["data"][count]["images"]["standard_resolution"]["url"].string!, forKey: "getPictureUrl")
                
                count=count+1
            }
        }) {
            (error) -> Void in
            print(error)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

       tableView.register(UINib(nibName: "TableViewCellGenius", bundle: nil), forCellReuseIdentifier: "cell")
        
        
        getPictureUrl()
        
       // saveDataWithoutInternet()
        
        if Reachability.isConnectedToNetwork(){
             debugPrint("Internet Connection Available!")
        }else{
            debugPrint("Internet Connection not Available!")
        }
        
        tableView.reloadData()
       
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 400.0
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if Reachability.isConnectedToNetwork(){
            getCount=1
        }
        else{
            getCount = DBManager.sharedInstance.getDataFromDB().count
        }
        return getCount
    }

  
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:TableViewCellGenius = (tableView.dequeueReusableCell(withIdentifier: "cell") as! TableViewCellGenius?)!
        if Reachability.isConnectedToNetwork(){
            
            let todoEndpoint = "https://api.instagram.com/v1/users/self/media/recent/?access_token=1403616281.1677ed0.4711f70fb8344bde931a09e56ef70514"
        
            self.requestGETURL(todoEndpoint, success: {
            (JSONResponse) -> Void in
                self.count=0
                let json=JSON(JSONResponse)
                while self.count<json["data"].count{
                    cell.loadImage.sd_setImage(with: URL(string:json["data"][self.count]["images"]["standard_resolution"]["url"].string!), placeholderImage:nil)
                    cell.lblCountLikes.text="  "+json["data"][self.count]["likes"]["count"].stringValue
                    cell.lblCountComments.text=" "+json["data"][self.count]["comments"]["count"].stringValue
                    self.count=self.count+1
                }
            }) {
            (error) -> Void in
                print(error)
            }
        }
        else{
            
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            var pictures = [CollieGalleryPicture]()
            let defaults = UserDefaults.standard
            let getPictureUrl = defaults.value(forKey: "getPictureUrl") as? String
            let url = getPictureUrl
            let picture = CollieGalleryPicture(url: url!, placeholder: nil, title: "Remote Image")
            pictures.append(picture)
            let gallery = CollieGallery(pictures: pictures)
            gallery.presentInViewController(self)
    }
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
